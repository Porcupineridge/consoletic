﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTic
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GamesPlayed { get; set; } = 0;
        public int GamesWon { get; set; } = 0;
        public int GamesDraw { get; set; } = 0;
        public int GamesLost { get; set; } = 0;
    }
}
