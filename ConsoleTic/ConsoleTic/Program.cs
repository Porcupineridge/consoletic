﻿using System;
using System.Threading;

namespace ConsoleTic
{

    class Program
    {
        static char[] arr = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public static Player Player1 { get; set; }
        public static Player Player2 { get; set; }
        static void Main(string[] args)
        {
            Register();
            Menu();
            
        }
        static void Register()
        {
            Console.WriteLine("Welcome to tic tac toe!");
            Console.WriteLine();
            Console.WriteLine("Player 1 enter your name:");
            Player1 = new Player { Name = Console.ReadLine() };
            Console.WriteLine("Player 2 enter your name");
            Player2 = new Player { Name = Console.ReadLine() };

        }

        private static void Menu()
        {
            Console.Clear();
            Console.WriteLine("Make a choice");
            Console.WriteLine();
            Console.WriteLine("1. Play New Game");
            Console.WriteLine("2. View Statistics");
            Console.WriteLine("3. Quit game");
            Console.WriteLine();
            string temp = Console.ReadLine();
            MenuChoice(temp);
        }

        private static void MenuChoice(string temp)
        {
            if (temp == "1")
                PlayGame();
            else if (temp == "2")
                ViewStatistics();
            else if (temp == "3")
                Console.WriteLine("Exit program");
            else
            {
                Console.WriteLine("Invalid input");
                Menu();
            }
        }

        private static void ViewStatistics()
        {
            Console.Clear();

            Console.WriteLine("Statistics");
            Console.WriteLine();

            Console.WriteLine($"{Player1.Name} statistics: ");
            Console.WriteLine();
            Console.WriteLine($"Wins: {Player1.GamesWon}");
            Console.WriteLine($"Losses: {Player1.GamesLost}");
            Console.WriteLine($"Draws: {Player1.GamesDraw}");
            Console.WriteLine();

            Console.WriteLine($"{Player2.Name} statistics: ");
            Console.WriteLine();
            Console.WriteLine($"Wins: {Player2.GamesWon}");
            Console.WriteLine($"Losses: {Player2.GamesLost}");
            Console.WriteLine($"Draws: {Player2.GamesDraw}");
            Console.WriteLine();

            Console.WriteLine("Press any key to return to Menu");
            Console.WriteLine();

            Console.ReadKey();
            
            Menu();
            

        }

        private static void PlayGame()
        {
            int turn = FlipACoin();
            MakeAMove(turn);
            CheckWon();
        }

        
        private static int FlipACoin()
        {
            Random rnd = new Random();
            int coin = rnd.Next(1, 3);
            if (coin == 1)
            {
                Console.WriteLine($"{Player1.Name } begins");
                return coin;
            }
            else
            {
                Console.WriteLine($"{Player2.Name} begins");
                return coin;
            }
        }
        private static void Board()
        {
            Console.Clear();

            Console.WriteLine("     |     |      ");

            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[1], arr[2], arr[3]);

            Console.WriteLine("_____|_____|_____ ");

            Console.WriteLine("     |     |      ");

            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[4], arr[5], arr[6]);

            Console.WriteLine("_____|_____|_____ ");

            Console.WriteLine("     |     |      ");

            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[7], arr[8], arr[9]);

            Console.WriteLine("     |     |      ");
        }
        private static void MakeAMove(int turn)
        {
            int move;
            bool checkWon = false;

            Board();

            while (checkWon == false)
            {

                if(turn == 1)
                {
                    Console.WriteLine($"{Player1.Name} make a move");
                    move = int.Parse(Console.ReadLine());
                    Player1Move(move);
                }
                else
                {

                    Console.WriteLine($"{Player2.Name} make a move");
                    move = int.Parse(Console.ReadLine());             
                    Player2Move(move);
                }
                
                
            }
        }

        private static void Player1Move(int move)
        {
            if(arr[move] == 'X')
            {
                Console.WriteLine("Already taken. Try again");
                Thread.Sleep(2000);
                MakeAMove(1);
            }
            else if (arr[move] == 'O')
            {
                Console.WriteLine("Already taken. Try again");
                Thread.Sleep(2000);
                MakeAMove(1);
            }
            else
            {
                
                arr[move] = 'X';
                MakeAMove(2);
            }
        }

        private static void Player2Move(int move)
        {

            if (arr[move] == 'X')
            {
                Console.WriteLine("Already taken. Try again");
                Thread.Sleep(2000);
                MakeAMove(2);
            }
                
            else if(arr[move] == 'O')
            {
                Console.WriteLine("Already taken. Try again");
                Thread.Sleep(2000);
                MakeAMove(2);

                
            }
            else
            {
                arr[move] = 'O';
                MakeAMove(1);
            }
        }

        

        private static void CheckWon()
        {
            //Player one winning conditions

            if(arr[1] == 'X' && arr[2] == 'X' && arr[3] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[4] == 'X' && arr[5] == 'X' && arr[6] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[7] == 'X' && arr[8] == 'X' && arr[9] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[1] == 'X' && arr[4] == 'X' && arr[7] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[2] == 'X' && arr[5] == 'X' && arr[8] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[3] == 'X' && arr[6] == 'X' && arr[9] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[1] == 'X' && arr[5] == 'X' && arr[9] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");
            else if (arr[3] == 'X' && arr[5] == 'X' && arr[7] == 'X')
                Console.WriteLine($"{Player1.Name} wins!");

            //Player two winning conditions

            if (arr[1] == 'O' && arr[2] == 'O' && arr[3] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[4] == 'O' && arr[5] == 'O' && arr[6] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[7] == 'O' && arr[8] == 'O' && arr[9] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[1] == 'O' && arr[4] == 'O' && arr[7] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[2] == 'O' && arr[5] == 'O' && arr[8] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[3] == 'O' && arr[6] == 'O' && arr[9] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[1] == 'O' && arr[5] == 'O' && arr[9] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");
            else if (arr[3] == 'O' && arr[5] == 'O' && arr[7] == 'O')
                Console.WriteLine($"{Player2.Name} wins!");


            
        }
    }
}
